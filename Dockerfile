ARG IMAGE_NAME
ARG IMAGE_TAG
FROM $IMAGE_NAME:$IMAGE_TAG
RUN <<EOF
apk update 
apk add --no-cache htop sysstat dns-tools screen busybox-extras
apk add --no-cache mysql-client postgresql14-client mongodb-tools
apk add --no-cache openldap-clients
apk add --no-cache aws-cli
EOF
CMD while true; do sleep 30; done;

